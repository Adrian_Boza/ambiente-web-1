<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $student = getStudent($id);
  if($student) {
    $deleted = deleteStudent($id);
    if($deleted) {
      header('Location: /workshop03/?status=success');
    } else {
      header('Location: /workshop03/?status=error');
    }
  } else {
    header('Location: /workshop03/?status=error');
  }
} else {
  header('Location: /workshop03/index.php');
}